from django.contrib.auth.forms import UserCreationForm
from accounts.models.user import User
from django import forms


class RegiserForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields = [
            "first_name",
            "last_name",
            "username",
            "email",
            "password",
            # "address",
            # "city",
            # "phone",
            "identity_number",
            # "post_code",
            # "is_company",
        ]
