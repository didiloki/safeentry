from django.db.models import fields
from accounts.models.user import *
from accounts.serializers.companyserializer import CompanySerializer
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = "__all__"


class CompanyVisitedSerializer(serializers.HyperlinkedModelSerializer):

    company_id = serializers.ReadOnlyField(source="company.id")
    company_address = serializers.ReadOnlyField(source="company.business_address")
    company_name = serializers.ReadOnlyField(source="company.business_name")
    company_address = serializers.ReadOnlyField(source="company.business_address")
    checked_in = serializers.ReadOnlyField()
    checked_out = serializers.ReadOnlyField()

    class Meta:
        model = CompanyUser
        fields = (
            "checked_in",
            "checked_out",
            "company_name",
            "company_address",
            "company_id",
        )


class GetVisitorSerializer(serializers.ModelSerializer):

    visited = CompanyVisitedSerializer(
        many=True,
        source="company_visited",
    )

    class Meta:
        model = User
        fields = (
            "first_name",
            "last_name",
            "username",
            "email",
            "address",
            "city",
            "phone",
            "identity_number",
            "post_code",
            "is_visitor",
            "visited",
        )
        read_only_fields = (
            "id",
            "visited",
        )


class VisitorSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            "first_name",
            "last_name",
            "username",
            "email",
            "password",
            "address",
            "city",
            "phone",
            "identity_number",
            "post_code",
            "is_visitor",
        )
        write_only_fields = ("password",)
        read_only_fields = ("id",)

    def create(self, validated_data):
        user = super().create(validated_data)
        user.is_visitor = True
        user.is_company = False
        # this is for hashing password
        user.set_password(validated_data["password"])
        user.save()
        return user


class CompanyVisitSerializer(serializers.ModelSerializer):
    class Meta:
        model = CompanyUser
        fields = "__all__"
