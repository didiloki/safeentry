# from accounts.models.user import User
import uuid
from django.db import models


class Company(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)  # new
    owner = models.ForeignKey(
        "accounts.User", on_delete=models.CASCADE, related_name="companies"
    )
    business_name = models.CharField(max_length=200)
    business_address = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    business_phone = models.CharField(max_length=50)
    business_email = models.EmailField(max_length=254)
    business_registration_number = models.CharField(max_length=50)

    is_active = models.BooleanField(default=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=False)

    # def __str__(self):
    #     return self.business_name
