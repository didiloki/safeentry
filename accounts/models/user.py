from accounts.models.company import Company
import uuid
from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    phone = models.CharField(max_length=50, unique=True)
    identity_number = models.CharField(max_length=50, unique=True)
    address = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    post_code = models.CharField(max_length=7, null=True)

    visited = models.ManyToManyField(
        Company,
        through="CompanyUser",
        through_fields=(
            "user",
            "company",
        ),
    )

    is_visitor = models.BooleanField("visitor status", default=False)
    is_company = models.BooleanField("company status", default=False)

    def full_name(self):
        return self.first_name + " " + self.last_name


class CompanyUser(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)  # new
    company = models.ForeignKey(
        Company, on_delete=models.CASCADE, related_name="company_visitors"
    )
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="company_visited"
    )

    checked_in = models.DateTimeField(auto_now_add=True)
    checked_out = models.DateTimeField(auto_now=True, null=True)

    # def __str__(self):
    #     return self.company.business_name + " : " + self.user.username
