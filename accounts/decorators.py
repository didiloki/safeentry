from django.core.exceptions import PermissionDenied

def admin_only(function):
    def wrapper(request, *args, **kwargs):
        if request.user and (request.user.is_admin and request.user.is_active):
            return function(request, *args, **kwargs)
        else:
            raise PermissionDenied
            
    wrapper.__doc__ = function.__doc__
    wrapper.__name__ = function.__name__
    return wrapper
