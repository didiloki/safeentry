from accounts.models.company import Company
from accounts.models import User, CompanyUser
from django.contrib import admin


class CompanyAdmin(admin.ModelAdmin):
    list_display = (
        "business_name",
        "city",
        "business_registration_number",
    )


# Register your models here.
admin.site.register(User)
admin.site.register(Company, CompanyAdmin)
admin.site.register(CompanyUser)
