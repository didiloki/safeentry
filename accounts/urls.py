from django.urls import path
from accounts.views import *

app_name = "accounts"

urlpatterns = [
    path("register/", register, name="register"),
    path("accounts/login/", login_view, name="user_login"),
    path("accounts/logout/", login_view, name="user_logout"),
    path("", dashboard_view, name="dashboard"),
]
