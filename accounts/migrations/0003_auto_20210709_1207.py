# Generated by Django 3.2.5 on 2021-07-09 12:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_user_post_code'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='identity_number',
            field=models.CharField(max_length=50, unique=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='phone',
            field=models.CharField(max_length=50, unique=True),
        ),
    ]
