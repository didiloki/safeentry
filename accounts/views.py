from accounts.forms import RegiserForm
from django.contrib import messages
from accounts.models.company import Company
from django.shortcuts import render, redirect
from rest_framework.response import Response
from rest_framework import exceptions
from accounts.models.user import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from rest_framework.decorators import api_view
import qrcode

# Create your views here.


def register(request):
    # qr = qrcode.QRCode(version=1, box_size=10, border=5)
    # company = Company.objects.all()
    # qr.add_data({"company": str(company[0].id), "dev": "ap1"})
    # qr.make(fit=True)
    # img = qr.make_image(fill_color="black", back_color="white").convert("RGB")
    # img.save("sample.jpg")
    if request.method == "POST":
        register_form = RegiserForm(request.POST)

        if register_form.is_valid():
            register.save()
            return redirect("accounts:dashboard")
    register_form = RegiserForm()
    context = {"register_form": register_form}
    return render(request, "accounts/register.html", context)


def login_view(request):
    if request.method == "POST":
        identity_number = request.POST.get("identity_number")
        password = request.POST.get("password")

        if (identity_number is None) or (password is None):
            messages.error(request, "No identity number or password")
            return redirect("accounts:user_login")

        user = User.objects.get(identity_number=identity_number)

        if user is None:
            messages.error(request, "user not found")
            return redirect("accounts:user_login")

        if not user.check_password(password):
            messages.error(request, "password incorrect")
            return redirect("accounts:user_login")

        login(request, user)
        return redirect("accounts:dashboard")

    return render(request, "accounts/login.html")


def logout_view(request):
    logout(request)
    return redirect("accounts:user_login")


@login_required
def dashboard_view(request):
    return render(request, "accounts/dashboard.html")
