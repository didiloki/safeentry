import React from 'react';
import {View,Text, Image} from "react-native";
import {Card, Button, Icon} from "react-native-elements";
import moment from "moment";

function StatusScreen({navigation, route}) {

  const color = route.params?.status === "checkin" ? "green" : 'blue'
  return (
      <View style={{flex: 1, justifyContent:"center"}}>
        <View style={{position: 'relative', justifyContent:"center"}}>
          <View style={{
            zIndex: 2,
            paddingBottom: 50,
            position: 'absolute', left: "39%", top:0,

          }}>

            <View style={{flexDirection: "row",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor:color,
              borderRadius: 50,
              width: 100,
              paddingVertical: 10

            }}>
              <Text style={{color: "white",
                fontWeight: "bold", marginRight: 10,
                fontSize: 19
              }}>1</Text>
              <Icon
                  name='user'
                  type='font-awesome'
                  color='white' />
            </View>

          </View>
          <View style={{height: 290,  zIndex: -1}}>
          <Card style={{flex: 1}}>
            <Card.Title style={{paddingTop: 50}}>Check In</Card.Title>
            <Card.Divider/>
              <Text style={{marginBottom: 10, fontSize:25, textAlign: "center"}}>
                {route.params ? route.params.companyName : "company Name"}
              </Text>
              <Text style={{marginBottom: 10, fontSize:25, textAlign: "center"}}>
                {moment().format("dddd MM YYYY")}{"\n"}
                {moment().format("hh:mm")}
              </Text>
            <Text style={{marginBottom: 10, textAlign: "center"}}>
              {route.params ? route.params.companyAddress : "company Address"}
            </Text>

          </Card>
          </View>

        </View>
        <View style={{ marginHorizontal: 16, marginVertical: 20}}>
          <Button
              icon={<Icon name='arrow-back' type='ionicon' color='#ffffff' />}
              onPress={() => navigation.navigate("Home")}
              buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
              title={'Back to Home'} />
        </View>
      </View>
  );
}

export default StatusScreen;
