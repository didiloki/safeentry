import React , {useState} from 'react';
import {View, Text, StyleSheet, TextInput, Image, TouchableOpacity} from "react-native";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import styles from "./authStyles"
import axios from "axios";
import {AuthContext} from "../../context/context";

function LoginScreen({navigation}) {
  const [phone, setPhone] = useState('')
  const [password, setPassword] = useState('')

  const onFooterLinkPress = () => {
    navigation.navigate('Register')
  }

  const { signIn } = React.useContext(AuthContext);

  return (
      <View style={styles.container}>
        <KeyboardAwareScrollView
            style={{ flex: 1, width: '100%' }}
            keyboardShouldPersistTaps="always">
          <Image
              style={styles.logo}
              source={require('../../assets/expose_icon.png')}
          />
          <TextInput
              style={styles.input}
              placeholder='Phone'
              placeholderTextColor="#aaaaaa"
              onChangeText={(text) => setPhone(text)}
              value={phone}
              underlineColorAndroid="transparent"
              autoCapitalize="none"
          />
          <TextInput
              style={styles.input}
              placeholderTextColor="#aaaaaa"
              secureTextEntry
              placeholder='Password'
              onChangeText={(text) => setPassword(text)}
              value={password}
              underlineColorAndroid="transparent"
              autoCapitalize="none"
          />

          <TouchableOpacity
              style={styles.button}
              onPress={() => signIn({phone, password})}>
            <Text style={styles.buttonTitle}>Login</Text>
          </TouchableOpacity>
          <View style={styles.footerView}>
            <Text style={styles.footerText}>Dont have an account? <Text onPress={onFooterLinkPress} style={styles.footerLink}>Log in</Text></Text>
          </View>
        </KeyboardAwareScrollView>
      </View>
  );
}


export default LoginScreen;
