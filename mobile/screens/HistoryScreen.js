import React, { useEffect, useState } from "react";
import {
  SafeAreaView,
  StyleSheet,
  SectionList,
  Text,
  View,
  ActivityIndicator,
} from "react-native";
import { useFocusEffect } from "@react-navigation/native";
import { BASE_URL, getProfile } from "../utilx/utiliz";
import * as SecureStore from "expo-secure-store";
import axios from "axios";
import moment from "moment";

function HistoryScreen({ navigation }) {
  const [history, setHistory] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useFocusEffect(
    React.useCallback(() => {
      let isActive = true;

      getHistory();
      async function getHistory() {
        console.log("useeffect");
        setIsLoading(true);
        try {
          let token = await SecureStore.getItemAsync("accessToken");
          let { data } = await axios.get(`${BASE_URL}/api/user/`, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          });

          const groups = data.visited.reduce((groups, game) => {
            const date = moment(game.checked_in.split("T")[0]).format(
              "dddd DD MMM"
            );
            if (!groups[date]) {
              groups[date] = [];
            }
            groups[date].push(game);
            return groups;
          }, {});
          // Edit: to add it in the array format instead
          const groupArrays = Object.keys(groups).map((date) => {
            return {
              date,
              data: groups[date],
            };
          });
          console.log(groupArrays);
          if (isActive) {
            setHistory(groupArrays);
            setIsLoading(false);
          }
        } catch (e) {
          console.log(e);
        }
      }

      return () => {
        isActive = false;
      };
    }, [])
  );
  // useEffect(() => {
  // async function getHistory() {
  //   console.log("useeffect");
  //   setIsLoading(true);
  //   try {
  //     let token = await SecureStore.getItemAsync("accessToken");
  //     let { data } = await axios.get(`${BASE_URL}/api/user/`, {
  //       headers: {
  //         Authorization: `Bearer ${token}`,
  //       },
  //     });
  //     // setHistory(data.visited)
  //     console.log(data.visited);
  //     const groups = data.visited.reduce((groups, game) => {
  //       const date = game.checked_in.split("T")[0];
  //       if (!groups[date]) {
  //         groups[date] = [];
  //       }
  //       groups[date].push(game);
  //       return groups;
  //     }, {});
  //     console.log("group", groups);
  //     // Edit: to add it in the array format instead
  //     const groupArrays = Object.keys(groups).map((date) => {
  //       return {
  //         date,
  //         data: groups[date],
  //       };
  //     });

  //     console.log("group Arrays", groupArrays);
  //     setHistory(groupArrays);
  //     setIsLoading(false);
  //   } catch (e) {
  //     console.log(e);
  //   }
  // }

  //   getHistory();
  // }, [navigation]);

  if (isLoading) {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text>Loading</Text>
        <ActivityIndicator />
      </View>
    );
  }
  return (
    <SafeAreaView style={{ flex: 1, marginTop: 40 }}>
      <View>
        <Text style={styles.padding15}>25 day history</Text>
        <SectionList
          sections={history}
          renderItem={({ item }) => (
            <View
              style={{
                flex: 1,
                backgroundColor: "#FFF",
                borderBottomWidth: 0.5,
                borderBottomColor: "#cccccc",
                paddingHorizontal: 15,
                paddingVertical: 10,
              }}
            >
              <Text style={[styles.item, { fontWeight: "bold" }]}>
                {item.company_name}
              </Text>
              <Text style={styles.item}>{item.company_address}</Text>

              <View></View>
            </View>
          )}
          renderSectionHeader={({ section }) => (
            <Text style={styles.sectionHeader}>{section.date}</Text>
          )}
          keyExtractor={(item, index) => index}
        />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22,
    // backgroundColor: "red",
  },
  sectionHeader: {
    paddingTop: 5,
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: 5,
    fontSize: 14,
    fontWeight: "bold",
    backgroundColor: "rgba(247,247,247,1.0)",
  },
  item: {
    fontSize: 18,
    // height: 44,
    color: "#616160",
  },
  padding15: {
    paddingHorizontal: 15,
    fontSize: 20,
    fontWeight: "bold",
    marginVertical: 15,
  },
});

export default HistoryScreen;
