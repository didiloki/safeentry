import React, {useEffect, useState} from 'react';
import {Text, View, StyleSheet, Button, TouchableOpacity} from "react-native";
import { BarCodeScanner } from 'expo-barcode-scanner';
import { Camera } from 'expo-camera';
import {BASE_URL, checkIn, isUUID} from "../utilx/utiliz";
import * as SecureStore from "expo-secure-store";
import axios from "axios";


function ScannerScreen({navigation}) {
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [type, setType] = useState(Camera.Constants.Type.back);

  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);

  const handleBarCodeScanned = async ({ type, data }) => {
    try {
      setScanned(true);
      setIsLoading(true)
      let dataParsed = JSON.parse(data.replace(/'/g, '"'))

      if("company" in dataParsed){
        // console.log("company in data")
        if(isUUID(dataParsed['company'])){
          // console.log(isUUID(dataParsed['company']))
          // let res = await checkIn({company: dataParsed.company})
          // console.log(res.data)
          let token = await SecureStore.getItemAsync('accessToken');
          let res = await axios.post(`${BASE_URL}/api/check_in_out/`,{company:dataParsed['company']+"" }, {
            headers:{
              Authorization: `Bearer ${token}`
            }})
          console.log(res.data)
          setIsLoading(false)
          navigation.navigate("Status", {"data" : dataParsed.company, status: "checkin"})
        }
      }
      //
      //
    }catch (e) {
      console.log("error checking in")
      setIsLoading(false)
      setScanned(false);
    }

    alert(`Bar code with type ${type} and data ${data} has been scanned!`);
  };

  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  if(isLoading){
    return <Text>Loading...</Text>
  }

  return (
      <View style={{ flex: 1, position: 'relative' }}>
            <View style={{ flex: 1, padding: 0 }}>
              <Camera
                  onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
                  style={{ flex:1}}
                  ratio={"16:9"}
                  barCodeScannerSettings={{
                    barCodeTypes: [BarCodeScanner.Constants.BarCodeType.qr],
                  }}
              />

            </View>
        <View style={{
            marginHorizontal: 20,
            padding: 20,
            opacity: 0.6,
            position: "absolute",
          backgroundColor:"black",
          width:"90%",
          bottom: 150

        }}>
          <Text style={{ color: "white"}}>By scanning this you agree:</Text>
          <Text style={{ color: "white"}}>
            - That you not sick with covid 19{"\n"}
            - You have no symptoms of covid
          </Text>
        </View>
      </View>

      //   <View style={{flex:1}}>
      //     {scanned && <Button title={'Tap to Scan Again'} onPress={() => setScanned(false)} />}
      //   </View>

  );
}

const styles = StyleSheet.create({
  buttonContainer:{},
  button:{},
  camera:{
    flex: 1
  }
});

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     flexDirection: 'column',
//     justifyContent: 'center',
//   },
//   absoluteFillObject:{
//     flex: 1
//   }
// })

export default ScannerScreen;
