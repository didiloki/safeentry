import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity, Image} from "react-native";
import { Ionicons } from '@expo/vector-icons';
import { Card, ListItem, Icon } from 'react-native-elements'
import { LinearGradient } from 'expo-linear-gradient';
import SvgUri from "expo-svg-uri";

function HomeScreen({navigation}) {
  return (
      <View style={{flex: 1}}>

          <LinearGradient
              colors={['#4c669f', '#3b5998', '#192f6a']}
              style={{ flex:1, paddingTop: 80 }}>
          <View style={{paddingHorizontal: 10, paddingBottom: 15}}>
            <Image
                resizeMode={'cover'}
                source={require('../assets/3877988.png')}
                style= {{
                  alignSelf: 'center',
                  height:'100%',
                  width:'100%'
                }}
            />
          </View>
          </LinearGradient>

       <View style={{flex: 2}}>
         <View style={styles.header}>
           <Text style={{fontWeight: "normal", fontSize: 20}}>Contact Tracing Check In</Text>
         </View>
         <Card style={styles.contactTracingOptions}>
           <View style={{flexDirection: "row", paddingTop: 20, justifyContent:"space-between"}}>
             <View style={{flex: 1}}>
               <TouchableOpacity
                   style={styles.button}
                   onPress={() => navigation.navigate('Scanner')}
                   // disabled={!this.state.isFormValid}
               >
                 <Ionicons name="scan-outline" size={25}
                           style={{marginBottom: 10}} />
                 <Text style={{fontSize: 17}}>
                   Scan
                 </Text>
               </TouchableOpacity>
             </View>
             <View style={{flex: 1}}>
               <TouchableOpacity
                   style={styles.button}
                   onPress={() => navigation.navigate('Favorites')}
                   // disabled={!this.state.isFormValid}
               >
                 <Ionicons name="ios-heart-sharp" size={25}
                           style={{marginBottom: 10}}
                 />
                 <Text style={{fontSize: 17}}>
                   Favorites
                 </Text>
               </TouchableOpacity>
             </View>
             <View style={{flex: 1}}>
               <TouchableOpacity
                   style={styles.button}
                   onPress={() => navigation.navigate('Favorites')}

               >
                 <Ionicons name="ios-person" size={25}
                           style={{marginBottom: 10}}
                  />
                 <Text style={{fontSize: 17}}>Group
                 </Text>
               </TouchableOpacity>
             </View>
           </View>
         </Card>

         <View style={styles.header}>
           <Text style={{fontWeight: "normal", fontSize: 20}}>Health Status</Text>
         </View>
         <Card style={[styles.contactTracingOptions, {marginTop: 0}]}>
           <View style={{flexDirection: "row", paddingTop: 20, justifyContent:"space-between"}}>
             <View style={[styles.button,{flex: 1}]}>
               <View style={{
                 marginBottom: 10,
                 backgroundColor: "green",
                  borderRadius: 50}}>
                 <Image
                     style={{width: 70, height: 70}}
                     source={require("../assets/vac_icon.png")} />
               </View>
                 <Text>Vaccinated
                 </Text>
             </View>
             <View style={[styles.button,{flex: 1}]}>
               <View style={{
                 marginBottom: 10,
                 backgroundColor: "gray",
                 borderRadius: 50}}>
                 <Image
                     style={{width: 70, height: 70}}
                     source={require("../assets/expose_icon.png")} />
               </View>
                 <Text>No Exposures
                 </Text>
             </View>
             <View style={{flex: 1}}>
               <TouchableOpacity
                   style={styles.button}
                   onPress={() => navigation.navigate('Scanner')}
                   // disabled={!this.state.isFormValid}
               >
                 <View style={{
                   marginBottom: 10,
                   backgroundColor: "green",
                   borderRadius: 50}}>
                   <Image
                       style={{width: 70, height: 70}}
                       source={require("../assets/vac_icon.png")} />
                 </View>
                 <Text >Covid Test Results
                 </Text>
               </TouchableOpacity>
             </View>
           </View>
         </Card>
       </View>
      </View>
  );
}

const styles = StyleSheet.create({
  contactTracingOptions: {
    // flex: 2,
    marginHorizontal: 20,
    paddingVertical:20,
    backgroundColor: "white",
    alignItems: "center"
  },
  button:{
    justifyContent: "center",
    alignItems: "center"
  },
  header:{
    marginHorizontal: 20,
    paddingVertical:20,
    // fontSize: 30,
    // fontWeight: "bold"
  }
})

export default HomeScreen;
