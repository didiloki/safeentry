import React from 'react';
import {FlatList, SafeAreaView, StyleSheet, Text, View} from "react-native";
import {Icon, Input} from "react-native-elements";

function FavoritesScreen(props) {
  return (
      <SafeAreaView style={{flex:1, paddingTop: 30}}>
      <View style={{marginHorizontal: 15}}>
        <Text style={{fontSize: 20}}>Check into favorites</Text>
        <Input />
      </View>
        <View style={styles.container}>
          <FlatList
              keyExtractor={item => item.companyName}
              data={[
                {companyName: 'Devin', companyAddress: "79 Anson road"},
                {companyName: 'Nigerian High Commission',
                  companyAddress: "77 Anson road"},
              ]}
              renderItem={({item}) => <FavoriteItem data={item} />}
          />
        </View>
      </SafeAreaView>
  );
}

function FavoriteItem({data}){
  return (
      <View style={[styles.row ]}>
        <View>
          <Text style={styles.h2}>{data.companyName}</Text>
          <Text style={styles.h3}>Address</Text>
        </View>
        <View>
          <Icon name={"star"}
                size={30}
                color={"#FFDF00"} />
        </View>
      </View>
  )
}
const styles = StyleSheet.create({
  container:{
    marginHorizontal : 10
  },
  h2:{
    fontSize: 19,
    fontWeight: "bold",
    marginBottom: 6
  },
  h3:{
    fontSize: 15,
    color: "gray",
  },
  row:{
    backgroundColor: "white",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 10,
    padding: 15,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  }
})
export default FavoritesScreen;
