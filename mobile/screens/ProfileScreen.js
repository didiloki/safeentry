import React, {useEffect} from 'react';
import {ListItem, Avatar, Button} from 'react-native-elements'
import {Image, SafeAreaView, Text, View} from "react-native";
import Gravatar from '@krosben/react-native-gravatar';
import {AuthContext} from "../context/context";

const list = [
  {
    name: 'Phone',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: '080123456'
  },
  {
    name: 'Identity Number',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: '123456'
  }
]

function ProfileScreen({route, navigation}) {
  const {getUser, signOut} = React.useContext(AuthContext);

  useEffect(() => {
    getUser()
  }, []);


  async function onLogout(){
    await signOut()
  }
  return (
  <SafeAreaView style={{flex: 1}}>
   <View style={{flex: 1, marginTop: 40,}}>
    <View style={{marginHorizontal: 10,
      marginTop: 40,
      marginBottom: 10,
      justifyContent: "center", alignItems: "center"}}>
      <Gravatar borderStyle={"rounded"} email="example@example.com" size={180} />
      <View style={{marginTop: 20, marginBottom: 10}}>
      <Text style={{textAlign: "center", fontSize: 20, fontWeight: "bold"}}>{route.params? route.params.first_name + " " + route.params.last_mame : "Ebere Schnecke"}</Text>
      <Text style={{textAlign: "center", fontSize: 16}}>Nationality: {route.params? route.params.first_name : "Unknown"} </Text>
      </View>
    </View>
      <View>
      {
        list.map((l, i) => (
            <ListItem key={i} bottomDivider>
              <Avatar source={{uri: l.avatar_url}} />
              <ListItem.Content>
                <ListItem.Title>{l.name}</ListItem.Title>
                <ListItem.Subtitle>{l.subtitle}</ListItem.Subtitle>
              </ListItem.Content>
            </ListItem>
        ))
      }
</View>
     <Text style={{margin: 15, fontSize: 20, fontWeight: "bold"}}>Help & Feedback </Text>
     <View>
       {
         list.map((l, i) => (
             <ListItem key={i} bottomDivider>
               <Avatar source={{uri: l.avatar_url}} />
               <ListItem.Content>
                 <ListItem.Title>Report Bug</ListItem.Title>
               </ListItem.Content>
             </ListItem>
         ))
       }
     </View>
   </View>
    <Button title={"logout"} onPress={() => onLogout()} />
  </SafeAreaView>
  );
}

export default ProfileScreen;
