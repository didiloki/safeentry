import axios from 'axios'
import * as SecureStore from "expo-secure-store";

export const BASE_URL = `https://sfeaproject.herokuapp.com`

/**
 *
 * @param company
 * @param data
 * @returns {Promise<AxiosResponse<any>>}
 */
export async function checkIn(company){
  let token = await SecureStore.getItemAsync('accessToken');
  return axios.post(`${BASE_URL}/api/check_in_out/`, JSON.stringify({ company }), {
    headers:{
      Authorization: `Bearer ${token}`
    }
  })
}

export async function getProfile(){
  let token = await SecureStore.getItemAsync('accessToken');
  return axios.get(`${BASE_URL}/api/user/`, {
    headers:{
      Authorization: `Bearer ${token}`
    }
  })
}

export function isUUID ( uuid ) {
  let s =(uuid).toString();

  s = s.match('^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$');
  if (s === null) {
    return false;
  }
  return true;
}

