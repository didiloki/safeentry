import { StatusBar } from 'expo-status-bar';
import React from 'react';
import {StyleSheet, Text, View, Button, ActivityIndicator} from 'react-native';

import 'react-native-gesture-handler';
import {AuthContext} from "./context/context";
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { Ionicons } from '@expo/vector-icons';
import HomeScreen from "./screens/HomeScreen";
import ScannerScreen from "./screens/ScannerScreen";
import ProfileScreen from "./screens/ProfileScreen";
import HistoryScreen from "./screens/HistoryScreen";
import FavoritesScreen from "./screens/FavoritesScreen";
import StatusScreen from "./screens/StatusScreen";
import RegisterScreen from "./screens/Auth/RegisterScreen";
import LoginScreen from "./screens/Auth/LoginScreen";
import axios from "axios";
import {BASE_URL, getProfile} from "./utilx/utiliz";
import * as SecureStore from "expo-secure-store";


const HomeStack = createStackNavigator();
function HomeStackScreen() {
  return (
      <HomeStack.Navigator>
        <HomeStack.Screen
            options={{headerShown: false}}
            name="Home" component={HomeScreen} />
        <HomeStack.Screen name="Scanner"
                          options={{headerShown: false}}
                          component={ScannerScreen} />
        <HomeStack.Screen name="Status"
                          options={{headerShown: false}}
                          component={StatusScreen} />
      </HomeStack.Navigator>
  );
}

const AuthStack = createStackNavigator();
function AuthStackScreen() {
  return (
      <AuthStack.Navigator>
        <AuthStack.Screen
            options={{headerShown: false}}
            name="Register" component={RegisterScreen} />
        <AuthStack.Screen name="Login"
                          options={{headerShown: false}}
                          component={LoginScreen} />
      </AuthStack.Navigator>
  );
}


const Tab = createBottomTabNavigator();

function BottomNavigationTab() {
  return (
      <Tab.Navigator
          screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {

              let iconName;
              if (route.name === 'Home') {
                iconName = focused
                    ? 'ios-home'
                    : 'ios-home-outline';
              } else if (route.name === 'History') {
                iconName = focused
                    ? 'ios-list'
                    : 'ios-list-outline';
              }else if (route.name === 'Favorites') {
                iconName = focused
                    ? 'ios-heart-sharp'
                    : 'ios-heart-outline';
              }else if (route.name === 'Profile') {
                iconName = focused
                    ? 'ios-person'
                    : 'ios-person-outline';
              }

              return (<Ionicons name={iconName} size={size} color={color} />)
            },
          })}
          tabBarOptions={{
            activeTintColor: 'tomato',
            inactiveTintColor: 'gray',
            labelStyle: {
              fontSize: 15
            }
          }}
      >
        <Tab.Screen name="Home" component={HomeStackScreen} />
        <Tab.Screen name="History" component={HistoryScreen} />
        <Tab.Screen name="Favorites" component={FavoritesScreen} />
        <Tab.Screen name="Profile" component={ProfileScreen} />
      </Tab.Navigator>
  );
}

export default function App() {
  const [state, dispatch] = React.useReducer(
      (prevState, action) => {
        switch (action.type) {
          case 'RESTORE_TOKEN':
            return {
              ...prevState,
              accessToken: action.tokens,
              isLoading: false,
            };
          case 'SIGN_IN':
            return {
              ...prevState,
              isSignOut: false,
              accessToken: action.tokens?.access,
            };
          case 'SIGN_OUT':
            return {
              ...prevState,
              isSignOut: true,
              accessToken: null,
            };
          case 'GET_PROFILE':
            return {
              ...prevState,
              isSignOut: false,
              accessToken: action.tokens?.access,
            };
        }
      },
      {
        isLoading: true,
        isSignOut: false,
        accessToken: null,
        user: null
      }
  );

  React.useEffect(() => {
    // Fetch the token from storage then navigate to our appropriate place
    const bootstrapAsync = async () => {
      let userToken;

      try {
        userToken = await SecureStore.getItemAsync('accessToken');
        dispatch({ type: 'RESTORE_TOKEN', tokens: userToken });
      } catch (e) {
        // Restoring token failed
      }

      // After restoring token, we may need to validate it in production apps

      // This will switch to the App screen or Auth screen and this loading
      // screen will be unmounted and thrown away.

    };

    bootstrapAsync();
  }, []);

  const authContext = React.useMemo(
      () => ({
        signIn: async cred => {
          try {
            let {data} = await axios.post(`${BASE_URL}/api/login/`, cred)
            console.log(data)
            await SecureStore.setItemAsync("accessToken", data.access);
            await SecureStore.setItemAsync("refreshToken", data.refresh);
            dispatch({ type: 'SIGN_IN', tokens: data });
          }catch (e) {
            console.log(e)
            console.log("fail")
          }


        },
        signOut: () => {
          SecureStore.deleteItemAsync("accessToken")
          dispatch({ type: 'SIGN_OUT' })
        },
        signUp: async data => {
          // In a production app, we need to send user data to server and get a token
          // We will also need to handle errors if sign up failed
          // After getting token, we need to persist the token using `SecureStore`
          // In the example, we'll use a dummy token

          dispatch({ type: 'SIGN_IN', token: 'dummy-auth-token' });
        },
        getUser : async () => {
          try{
             let user = await getProfile()

          }catch (e) {

          }
        }
      }),
      []
  );

  if(state.isLoading){
    return (
        <ActivityIndicator />
    )
  }
  return (
      <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        {state.accessToken == null ?
            (<AuthStackScreen/>) :
            (<BottomNavigationTab/>)
        }
      </NavigationContainer>
        </AuthContext.Provider>
  );
}

