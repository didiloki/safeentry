from accounts.models.user import User
from accounts.models.company import Company
from accounts.serializers.companyserializer import *
from accounts.serializers.companyvisitedserializer import *
from django.shortcuts import render
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework import exceptions, status
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.tokens import AccessToken, RefreshToken
import json

# Create your views here.
@api_view(["POST"])
def register_users_api(request):
    """
    Visitor registration this function takes
    { first_name, last_name, email, phone, identity_number, address, city, postcode}
    sample data
    {
        "email" : "ebere@ga.co",
        "username" : "ebere",
        "password" : "ebe123@g",
        "phone" : "2065036980",
        "address" : "192 millie road",
        "city" : "singapore",
        "post_code" : "520103",
        "first_name" : "ebere",
        "last_name" : "ebere",
        "identity_number" : "1234567"
        }
    """
    visitor = VisitorSerializer(data=request.data)
    if visitor.is_valid():
        visitor.save()
        return Response(
            {"details": "visitor registered"}, status=status.HTTP_201_CREATED
        )
    else:
        return Response(
            {"details": dict(visitor.errors)}, status=status.HTTP_400_BAD_REQUEST
        )


@api_view(["POST"])
def login_user_api(request):
    """
    {"password" : "ebe123@g",
    "phone" : "2065036980"
    }
    """
    # get data from req object
    phone = request.data.get("phone")
    password = request.data.get("password")

    if not phone or not password:
        raise exceptions.AuthenticationFailed

    user = User.objects.get(phone=phone)

    if user is None:
        raise exceptions.AuthenticationFailed("user not found")
    if not user.check_password(password):
        raise exceptions.AuthenticationFailed("password is not a match")

    refresh = RefreshToken.for_user(user)

    return Response({"refresh": str(refresh), "access": str(refresh.access_token)})


@api_view(["GET"])
@permission_classes([IsAuthenticated])
def visitor_data(request):
    try:
        user = User.objects.get(pk=request.user.id)
    except:
        return Response(
            {"detail": "missing visitor"}, status=status.HTTP_401_UNAUTHORIZED
        )

    serialized_user = GetVisitorSerializer(user, many=False)
    print(serialized_user.data["visited"][0].__dict__)
    return Response(serialized_user.data, status=status.HTTP_200_OK)


@api_view(["POST"])
@permission_classes([IsAuthenticated])
def visitor_checkin_checkout(request):
    """
    expects to receive { "company" : "b77bbca8-512f-4d9c-9254-618ef77ae623", "type" : "check_in" }
    and find company if exists and then add
    """
    print(request.data["company"])
    # body_of_request = json.loads(request.body)
    try:
        # pass
        company = Company.objects.get(pk=request.data["company"])

    except Company.DoesNotExist:
        return Response(
            {"detail": "company in records"}, status=status.HTTP_403_FORBIDDEN
        )

    request.data["user"] = request.user.id
    company_visitor = CompanyVisitSerializer(data=request.data)

    if company_visitor.is_valid():
        company_visitor.save()

        # company.
        return Response({}, status=status.HTTP_200_OK)
    else:
        print(company_visitor.errors)
        return Response({}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["DELETE"])
@permission_classes([IsAuthenticated])
def visitor_logout(request):
    try:
        refresh_token = request.data["refresh_token"]
        token = RefreshToken(refresh_token)
        token.blacklist()

        return Response(status=status.HTTP_205_RESET_CONTENT)
    except Exception as e:
        return Response(status=status.HTTP_400_BAD_REQUEST)
