from django.urls import path
from apis.views import *

app_name = "apis"

urlpatterns = [
    path("register/", register_users_api, name="visitor_register_api"),
    path("login/", login_user_api, name="visitor_login_api"),
    path("user/", visitor_data, name="visitor_data_api"),
    path("check_in_out/", visitor_checkin_checkout, name="visitor_checkin_out_api"),
    # path("check_out/", check_out_view, name="visitor_checkout"),
]
