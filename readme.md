## Covid 19 contact tracing app

This app was completed with :

1.  Django backend
1.  React Native frontend

### Features

1. Users can scan qr codes to checkin to location
2. Company owners can view all company total amount of visits per day/ week/month

### Screenshots

![](simulator.png)

### Feature build

1. User can group checkin for friends and family
2. User can use app to show covid vaccine certificate as proof of completed jab.
